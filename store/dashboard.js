export const state = () => ({
    loan: null,
    history: null,
    wallet: null,
    invest: null,
    marketplace: null,
});

export const mutations = {
    setLoan(state, loan) {
        state.loan = loan;
    },
    setHistory(state, history) {
        state.history = history;
    },
    setWallet(state, wallet) {
        state.wallet = wallet;
    },
    setInvest(state, invest) {
        state.invest = invest;
    },
    setMarketplace(state, marketplace) {
        state.marketplace = marketplace;
    },
    clear(state) {
        state.loan = null;
        state.history = null;
    }
};

export const getters = {
    loan(state) {
        return state.loan;
    },
    history(state) {
        return state.history
    },
    wallet(state) {
        return state.wallet
    },
    invest(state) {
        return state.invest
    },
    marketplace(state) {
        return state.marketplace
    },
}