export const state = () => ({
    lender: {},
    settings: {},
    data: {
        avatar: null,
        loan: {
            tenor_type: 3,
            duration: 0
        },
        profile: {},
        nok: {},
        income: {},
        account: {},
        card: {},
    },
    invest: {
        profile: {},
        investment: {
            manager: "1"
        },
        nok: {},
        account: {}
    }
});

export const mutations = {
    updateData(state, update) {
        state.data = { ...state.data, ...update };
    },
    setLender(state, lender) {
        state.lender = lender;
    },
    setSettings(state, settings) {
        state.settings = settings;
    },
    setData(state, data) {
        state.data = data;
    },
    setInvest(state, invest) {
        state.invest = invest;
    }
};

export const getters = {
    data(state) {
        return state.data;
    },
    lender(state) {
        return state.lender
    },
    settings(state) {
        return state.settings
    },
    invest(state) {
        return state.invest
    }
}