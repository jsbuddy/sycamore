export const state = () => ({
    requests: [],
    investments: [],
    current: null,
    investing: false,
});

export const mutations = {
    updateData(state, update) {
        state.data = { ...state.data, ...update };
    },
    setRequests(state, requests) {
        state.requests = requests;
    },
    setCurrent(state, current) {
        state.current = current;
    },
    setInvesting(state, investing) {
        state.investing = investing;
    },
    setInvestments(state, investments) {
        state.investments = investments;
    },
};

export const getters = {
    data(state) {
        return state.data;
    },
    requests(state) {
        return state.requests
    },
    current(state) {
        return state.current
    },
    investing(state) {
        return state.investing
    },
    investments(state) {
        return state.investments
    },
}