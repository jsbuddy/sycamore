export default {
    data() {
        return {
            modals: {
                fund: {},
                withdraw: {}
            }
        }
    },
    methods: {
        addModal(name) {
            this.modals[name] = { visible: false }
        },
        openModal(name) {
            this.modals[name].visible = true;
        },
        closeModal(name) {
            this.modals[name].visible = false;
        },
    }
}
