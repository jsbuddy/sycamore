import Vue from 'vue'
import VueFriendlyIframe from 'vue-friendly-iframe';

export default () => {
    Vue.component('vue-friendly-iframe', VueFriendlyIframe);
}
