const apiKey = process.env.apiKey;

export default function ({ $axios, redirect }) {
    $axios.onRequest(config => {
        config.headers = {
            "x-api-key": apiKey,
            "Accept": "application/json"
        };
        return config;
    })
}
