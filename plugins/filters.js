import Vue from 'vue'

export function timeAgo(time) {
    const between = Date.now() / 1000 - Number(time)
    if (between < 3600) {
        return pluralize(~~(between / 60), ' minute')
    } else if (between < 86400) {
        return pluralize(~~(between / 3600), ' hour')
    } else {
        return pluralize(~~(between / 86400), ' day')
    }
}

export function currency(value) {
    if (!value || isNaN(value)) return 0;
    const result = (+value).toLocaleString(undefined, {
        maximumFractionDigits: 2
    });
    return `₦${result}`
}

const filters = { timeAgo, currency }

export default Object.keys(filters).forEach(key => {
    Vue.filter(key, filters[key])
})