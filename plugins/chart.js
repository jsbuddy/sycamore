import Vue from 'vue'
import { Pie } from 'vue-chartjs'

Vue.component("pie-chart", {
    extends: Pie,
    props: ["options", "data"],
    mounted() {
        this.renderChart(this.data, this.options);
    }
});