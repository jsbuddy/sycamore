export default function ({ store, redirect }) {
    if (!store.state.marketplace.current) {
        return redirect('/marketplace')
    }
}