const capitalize = (s) => {
    if (typeof s !== 'string') return ''
    return s.charAt(0).toUpperCase() + s.slice(1)
}

export const validate = (field, value, messages) => {
    if (!value) {
        messages[field] = `${capitalize(field)} is required!`;
        return messages;
    } else {
        if (messages.hasOwnProperty(field)) messages[field] = '';
    }
    if (field === 'phone') {
        if (!value.match(/^[0]\d{10}$/gi)) messages[field] = 'Invalid phone number'
    }
    if (field === 'email') {
        if (!value.match(/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/gi)) messages[field] = 'Invalid email address'
    }
    return messages;
};

export const validateAll = (fields, data) => {
    let messages = {};
    fields.forEach(field => {
        messages = validate(field, data[field], messages);
    })
    return messages;
};
