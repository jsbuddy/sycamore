
export default {
  mode: 'universal',
  /*
  ** Headers of the page
  */
  head: {
    title: 'Get Loans, Invest & Earn - Sycamore.ng',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: 'Sycamore is a secured Peer-to-Peer Lending Platform to get access to loans (personal or business loans) and to grow your wealth by earning from lending to borrowers.' },
      { hid: 'keywords', name: 'keywords', content: 'quick loan in nigeria, quick cash loan, quick online loans in nigeria, quick cash loan code, fast cash loans lagos, 24hrs loan in nigeria, petty cash loan, loan, Saving Money in Nigeria, Savings Account, Periodic savings, Fixed savings, Onetime savings,  Meristem Trustees, Digital savings, Savings Automation, High interest rate, Save Money, FinTech, Online savings platform, Digital savings in Nigeria, Personal Finance, Investments, Wealth Management, savings account interest rate, saving account interest rate calculator, savings account with high interest, savings account promotions, monthly savings account, payday investor, payday automated savings, digital piggy bank, payday online investment, fixed savings rate, fixed savings high interest, fixed deposit, fixed deposit account, money market, bonds, treasury bills' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: 'favicon.ico' },
      { rel: 'stylesheet', href: 'https://fonts.googleapis.com/css?family=Poppins:400,500,600&display=swap' },
      { rel: 'stylesheet', href: '/css/bootstrap.min.css' },
      { rel: 'stylesheet', href: 'https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2//css/all.min.css' },
      { rel: 'stylesheet', href: '/css/flaticon.css' },
      { rel: 'stylesheet', href: '/css/owl.carousel.min.css' },
      { rel: 'stylesheet', href: '/css/owl.theme.default.min.css' },
      { rel: 'stylesheet', href: '/css/style.css' },
    ],
    script: [
      { src: 'https://www.googletagmanager.com/gtag/js?id=UA-158762007-1' },
      { src: '/js/ga.js' },
      { src: '/js/jquery-3.3.1.min.js' },
      { src: '/js/bootstrap.min.js' },
      { src: '/js/owl.carousel.min.js' },
      { src: 'https://unpkg.com/vue-paystack/dist/paystack.min.js' },
    ],
  },
  loading: { color: '#e23b33' },
  css: [
  ],
  plugins: [
    '~/plugins/filters',
    '~/plugins/axios',
    '~/plugins/vue-friendly-iframe',
    '~/plugins/chart',
  ],
  buildModules: [
  ],
  modules: [
    'nuxt-buefy',
    '@nuxtjs/axios',
    '@nuxtjs/pwa',
    '@nuxtjs/dotenv',
    '@nuxtjs/auth'
  ],
  axios: {
    baseURL: 'https://mobile.creditclan.com/webapi/v1/',
    progress: true
  },
  env: {
    apiKey: '6f2oyFArftQsk2NY5XymekadAGehmCz9oWIprcbpYc3Hoq',
  },
  auth: {
    strategies: {
      local: {
        endpoints: {
          login: true,
          user: true,
          logout: false
        },
      }
    }
  },
  build: {
    extend(config, ctx) {
    }
  }
}
